# Ionic Angular challenge!

[Running project here! (deployed to GitLab pages)](https://galvarin91.gitlab.io/ionic-angular-challenge/)

## Stack
- Angular
- RxJS
- Ionic
- Bootstrap
- Sass
- CSS Grid
- CSS Flexbox
- GitLab CI/CD
- GitLab Pages
- Jasmine

## Features
- Modules
  > Created different modules encapsulating specific functionality
- Lazy loading
  > Took advantage of lazy loading to load modules only when are needed
- Unit testing
  > Created Unit Testing examples using Jasmine, teams.service.spec, competitions.service.spec, header.component.spec
- Responsive
  > Used CSS Grid, flexbox and media query to display the interface according to the display
- CI/CD
  > Used GitLab to introduce CI/CD, included stages for install, testing and deploy through GitLab pages
- Competitions list with season filter
  > Created competitions page, included filter by season year and a toggle to display free/paid competitions info!
- TeamList
  > Display a team list based on a selected competition
- TeamDetail
  > Display a selected team detail and its players

## Project structure fundamentals
```
src/    
├── app/
│   ├── core/ 
│   │   ├── core.module.ts 
│   │   ├── guards/ 
│   │   ├── interceptors/ 
│   │   ├── models/ 
│   │   └── services/ 
│   ├── modules/ 
│   │   ├── module-a/ 
│   │   │   ├── module-a.module.ts
│   │   │   ├── module-a-routing.module.ts
│   │   │   ├── components/
│   │   │   ├── modules/
│   │   │   │   ├── sub-module-a/
│   │   │   │   │   ├── sub-module-a.module.ts
│   │   │   │   │   ├── sub-module-a-routing.module.ts
│   │   │   │   │   ├── components/
│   │   │   │   │   │   ├── sub-module-a-component-1/
│   │   │   │   │   │   ├── sub-module-a-component-2/
│   │   │   │   │   │   └── ..
│   │   │   │   │   ├── modules/
│   │   │   │   │   ├── pages/
│   │   │   │   │   │   ├── sub-module-a/
│   │   │   │   │   │   ├── sub-module-a-detail/
│   │   │   │   │   │   └── ..
│   │   │   │   │   └── services/
│   │   │   │   ├── sub-module-b/
│   │   │   │   └── ..
│   │   │   ├── pages/
│   │   │   └── services/
│   │   ├── module-b/
│   │   └── ..
│   └── shared/
│       ├── shared.module.ts 
│       ├── components/ 
│       ├── directives/ 
│       └── pipes/
├── assets/
│   ├── styles/ 
│   │   ├── global.scss
│   │   ├── themes/
│   │   └── ..
│   ├── img/ 
│   └── ..
└── environments/
```

The **core module** should be loaded directly by app.module, not lazy loaded, it should be imported only once in the whole app.
> Services stored in this folder must be those required throughout the application.

The **modules** directory includes each feature module (well-defined piece of functionality) that needs to be lazy loaded, usually linked to a specific business logic. Modules can include sub-modules if they become too large or to take advantage of lazy loading.
> - Components folder includes all the reusable components needed to create pages in the module.
> - Pages within modules are those components that do not have a selector and are only referenced by a routing module.
> - Services are only loaded when the module itself is loaded (provided only for the module), can be used to save a state for the module or distribute data between components.

The **shared module** is also eager loaded by app.module, it must also be imported by each module that needs it. Necessary imports throughout the application should be done here to avoid duplicates.
> Components, directives and pipes created here should not be tied to any specific module or business logic, they should be reusable throughout the application.

