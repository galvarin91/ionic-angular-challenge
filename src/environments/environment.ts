// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://api.football-data.org/v2',
  apiToken: '5f7d022d807c46a89c578a02e5055893',
  availableCompetitionCodes: [
    'BSA',
    'PL',
    'ELC',
    'EC',
    'CL',
    'FL1',
    'BL1',
    'SA',
    'DED',
    'PPL',
    'CLI',
    'PD',
    'WC'
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
