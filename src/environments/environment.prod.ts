export const environment = {
  production: true,
  apiUrl: 'https://api.football-data.org/v2',
  apiToken: '5f7d022d807c46a89c578a02e5055893',
  availableCompetitionCodes: [
    'BSA',
    'PL',
    'ELC',
    'EC',
    'CL',
    'FL1',
    'BL1',
    'SA',
    'DED',
    'PPL',
    'CLI',
    'PD',
    'WC'
  ]
};
