import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ITableHeader } from './table';

@Component({
  selector: 'app-table',
  templateUrl: 'table.component.html',
  styleUrls: ['table.component.scss']
})
export class TableComponent {
  @Input() headers: ITableHeader[];
  @Input() rows: any[];
  @Input() loading: boolean;
  @Output() rowClick = new EventEmitter<any>();

  constructor() {}

  onRowClick(row) {
    this.rowClick.emit(row);
  }
}
