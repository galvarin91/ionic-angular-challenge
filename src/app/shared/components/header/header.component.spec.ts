import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { MenuController } from '@ionic/angular';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let debugElement: DebugElement;
  const menuSpy = jasmine.createSpyObj('MenuController', ['open']);

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [HeaderComponent],
        providers: [{ provide: MenuController, useValue: menuSpy }],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display title: Header', () => {
    component.title = 'header';
    fixture.detectChanges();
    const header = debugElement.queryAll(By.css('ion-title'));
    expect(header.length).toBe(1);

    const headerText = header[0].nativeElement.innerHTML.trim();
    expect(headerText).toBe('header');
  });

  it('should display menu button by default', () => {
    const menuButton = debugElement.queryAll(By.css('ion-menu-button'));
    const backButton = debugElement.queryAll(By.css('ion-back-button'));

    expect(menuButton.length).toBe(1);
    expect(backButton.length).toBe(0);
  });

  it('should hide menu button and display backButton', () => {
    component.showBackButton = true;
    fixture.detectChanges();
    const menuButton = debugElement.queryAll(By.css('ion-menu-button'));
    const backButton = debugElement.queryAll(By.css('ion-back-button'));

    expect(menuButton.length).toBe(0);
    expect(backButton.length).toBe(1);
  });

  it('should open menu', () => {
    component.openMenu();
    fixture.detectChanges();

    expect(menuSpy.open).toHaveBeenCalled();
  });
});
