import { Component, Input } from '@angular/core';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent {
  @Input() title: string;
  @Input() showBackButton: boolean;

  constructor(private menu: MenuController) {}

  openMenu() {
    this.menu.open();
  }
}
