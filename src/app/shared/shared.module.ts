import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { TableComponent } from './components/table/table.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [HeaderComponent, TableComponent],
  exports: [
    HeaderComponent,
    TableComponent,
    CommonModule,
    ReactiveFormsModule,
    IonicModule
  ],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule]
})
export class SharedModule {}
