import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CompetitionsTableComponent } from './competitions-table.component';

describe('CompetitionsTableComponent', () => {
  let component: CompetitionsTableComponent;
  let fixture: ComponentFixture<CompetitionsTableComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [CompetitionsTableComponent],
        imports: [IonicModule.forRoot()]
      }).compileComponents();

      fixture = TestBed.createComponent(CompetitionsTableComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
