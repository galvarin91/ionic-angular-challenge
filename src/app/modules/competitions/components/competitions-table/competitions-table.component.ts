import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ICompetition } from 'src/app/core/models/competition';
import { ITableHeader } from 'src/app/shared/components/table/table';

@Component({
  selector: 'app-competitions-table',
  templateUrl: './competitions-table.component.html',
  styleUrls: ['./competitions-table.component.scss']
})
export class CompetitionsTableComponent {
  headers: ITableHeader[] = [
    { label: 'Id', key: 'id' },
    { label: 'Name', key: 'name' },
    { label: 'Country', key: 'country' },
    { label: 'Current season year', key: 'currentSeasonYear' }
  ];
  @Input() competitions: ICompetition[];
  @Input() loading: boolean;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  onRowClick($event: ICompetition) {
    this.router.navigate([$event.id, $event.currentSeasonYear, 'teams'], {
      relativeTo: this.activatedRoute
    });
  }
}
