import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompetitionsPage } from './pages/competitions/competitions.page';

const routes: Routes = [
  {
    path: '',
    component: CompetitionsPage
  },
  {
    path: ':competitionId/:seasonYear/teams',
    loadChildren: () =>
      import('./modules/teams/teams.module').then((m) => m.TeamsModule)
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompetitionsRoutingModule {}
