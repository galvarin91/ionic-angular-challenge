import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuController, ViewWillEnter } from '@ionic/angular';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ICompetition } from 'src/app/core/models/competition';
import { CompetitionsService } from 'src/app/core/services/competitions.service';

@Component({
  templateUrl: './competitions.page.html',
  styleUrls: ['./competitions.page.scss']
})
export class CompetitionsPage implements OnInit, ViewWillEnter {
  competitions$: Observable<ICompetition[]>;
  loading = true;
  searchFG: FormGroup;
  showFreeTierData: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private competitionsService: CompetitionsService,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.searchFG = this.formBuilder.group(
      {
        seasonYear: [
          '',
          [Validators.minLength(4), Validators.pattern('^[0-9]*$')]
        ]
      },
      { updateOn: 'submit' }
    );

    this.competitions$ = this.competitionsService.competitions$.pipe(
      tap(() => (this.loading = false))
    );
  }

  ionViewWillEnter() {
    this.menu.enable(true);
  }

  get seasonYear() {
    return this.searchFG.get('seasonYear');
  }

  onSearchFormSubmit() {
    this.filterCompetitions();
  }

  onToggleChange($event) {
    this.showFreeTierData = $event.detail.checked;
    this.filterCompetitions();
  }

  private filterCompetitions() {
    if (this.searchFG.valid) {
      this.loading = true;
      this.competitionsService.filterCompetitions(
        this.seasonYear.value,
        this.showFreeTierData
      );
    }
  }
}
