import { NgModule } from '@angular/core';
import { CompetitionsRoutingModule } from './competitions-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompetitionsPage } from './pages/competitions/competitions.page';
import { CompetitionsTableComponent } from './components/competitions-table/competitions-table.component';

@NgModule({
  declarations: [CompetitionsPage, CompetitionsTableComponent],
  imports: [CompetitionsRoutingModule, SharedModule]
})
export class CompetitionsModule {}
