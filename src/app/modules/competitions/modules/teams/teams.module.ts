import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { TeamMembersTableComponent } from './components/team-members-table/team-members-table.component';
import { TeamsTableComponent } from './components/teams-table/teams-table.component';
import { TeamDetailPage } from './pages/team-detail/team-detail.page';
import { TeamsPage } from './pages/teams/teams.page';
import { TeamsRoutingModule } from './teams-routing.module';

@NgModule({
  declarations: [
    TeamsPage,
    TeamsTableComponent,
    TeamDetailPage,
    TeamMembersTableComponent
  ],
  imports: [TeamsRoutingModule, SharedModule]
})
export class TeamsModule {}
