import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TeamsTableComponent } from './teams-table.component';

describe('TeamsTableComponent', () => {
  let component: TeamsTableComponent;
  let fixture: ComponentFixture<TeamsTableComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TeamsTableComponent],
        imports: [IonicModule.forRoot()]
      }).compileComponents();

      fixture = TestBed.createComponent(TeamsTableComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })
  );

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
