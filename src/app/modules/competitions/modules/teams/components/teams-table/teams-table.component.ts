import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ITeam } from 'src/app/core/models/team';
import { ITableHeader } from 'src/app/shared/components/table/table';

@Component({
  selector: 'app-teams-table',
  templateUrl: './teams-table.component.html',
  styleUrls: ['./teams-table.component.scss']
})
export class TeamsTableComponent {
  headers: ITableHeader[] = [
    { label: 'Id', key: 'id' },
    { label: 'Name', key: 'name' },
    { label: 'Country', key: 'country' },
    { label: 'Club colors', key: 'clubColors' }
  ];
  @Input() teams: ITeam[];

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  onRowClick($event: ITeam) {
    this.router.navigate([$event.id], {
      relativeTo: this.activatedRoute
    });
  }
}
