import { Component, Input } from '@angular/core';
import { ITeamMember } from 'src/app/core/models/team';
import { ITableHeader } from 'src/app/shared/components/table/table';

@Component({
  selector: 'app-team-members-table',
  templateUrl: './team-members-table.component.html',
  styleUrls: ['./team-members-table.component.scss']
})
export class TeamMembersTableComponent {
  headers: ITableHeader[] = [
    { label: 'Id', key: 'id' },
    { label: 'Name', key: 'name' },
    { label: 'Position', key: 'position' },
    { label: 'Date of birth', key: 'dateOfBirth' },
    { label: 'Nationality', key: 'nationality' }
  ];
  @Input() teamMembers: ITeamMember[];

  constructor() {}
}
