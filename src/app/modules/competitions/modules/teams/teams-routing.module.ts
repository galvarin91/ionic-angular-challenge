import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamDetailPage } from './pages/team-detail/team-detail.page';
import { TeamsPage } from './pages/teams/teams.page';

const routes: Routes = [
  {
    path: '',
    component: TeamsPage
  },
  {
    path: ':teamId',
    component: TeamDetailPage
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamsRoutingModule {}
