import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { ITeamDetail } from 'src/app/core/models/team';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
  templateUrl: './team-detail.page.html',
  styleUrls: ['./team-detail.page.scss']
})
export class TeamDetailPage implements OnInit {
  teamDetail$: Observable<ITeamDetail>;
  loading = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private teamsService: TeamsService,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.menu.enable(false);
    this.teamDetail$ = this.activatedRoute.params.pipe(
      switchMap((routeParams) =>
        this.teamsService
          .getTeamDetail(routeParams.teamId)
          .pipe(tap(() => (this.loading = false)))
      )
    );
  }
}
