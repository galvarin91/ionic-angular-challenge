import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { ITeam } from 'src/app/core/models/team';
import { TeamsService } from 'src/app/core/services/teams.service';

@Component({
  templateUrl: './teams.page.html',
  styleUrls: ['./teams.page.scss']
})
export class TeamsPage implements OnInit {
  teams$: Observable<ITeam[]>;
  loading = true;
  restrictedResourceError: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private teamsService: TeamsService,
    private menu: MenuController
  ) {}

  ngOnInit() {
    this.menu.enable(false);
    this.teams$ = this.activatedRoute.params.pipe(
      switchMap((routeParams) =>
        this.teamsService
          .getTeams(routeParams.competitionId, routeParams.seasonYear)
          .pipe(
            tap(() => (this.loading = false)),
            catchError((error: HttpErrorResponse) => {
              if (error.status === 403) {
                this.restrictedResourceError = true;
              }
              this.loading = false;
              return of([]);
            })
          )
      )
    );
  }
}
