import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss']
})
export class HomePage {
  stackList: string[] = [
    'Angular',
    'RxJS',
    'Ionic',
    'Bootstrap',
    'Sass',
    'CSS Grid',
    'CSS Flexbox',
    'GitLab CI/CD',
    'GitLab Pages',
    'Jasmine'
  ];

  featureList: { title: string; description: string }[] = [
    {
      title: 'Modules',
      description:
        'Created different modules encapsulating specific functionality'
    },
    {
      title: 'Lazy loading',
      description:
        'Took advantage of lazy loading to load modules only when are needed'
    },
    {
      title: 'Unit testing',
      description:
        'Created Unit Testing examples using Jasmine, teams.service.spec, competitions.service.spec, header.component.spec'
    },
    {
      title: 'Responsive',
      description:
        'Used CSS Grid, flexbox and media query to display the interface according to the display'
    },
    {
      title: 'CI/CD',
      description:
        'Used GitLab to introduce CI/CD, included stages for install, testing and deploy through GitLab pages'
    },
    {
      title: 'Competitions list with season filter',
      description:
        'Created competitions page, included filter by season year and a toggle to display free/paid competitions info!'
    },
    {
      title: 'TeamList',
      description: 'Display a team list based on a selected competition'
    },
    {
      title: 'TeamDetail',
      description: 'Display a selected team detail and its playes'
    }
  ];

  constructor() {}
}
