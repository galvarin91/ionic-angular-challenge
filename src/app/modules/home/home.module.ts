import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { HomePage } from './pages/home/home.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HomePage],
  imports: [HomeRoutingModule, SharedModule]
})
export class HomeModule {}
