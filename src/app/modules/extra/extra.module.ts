import { NgModule } from '@angular/core';
import { ExtraRoutingModule } from './extra-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ExtraPage } from './pages/home/extra.page';

@NgModule({
  declarations: [ExtraPage],
  imports: [ExtraRoutingModule, SharedModule]
})
export class ExtraModule {}
