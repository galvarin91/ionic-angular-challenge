import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExtraPage } from './pages/home/extra.page';

const routes: Routes = [
  {
    path: '',
    component: ExtraPage
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtraRoutingModule {}
