import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'competitions',
    loadChildren: () =>
      import('./modules/competitions/competitions.module').then(
        (m) => m.CompetitionsModule
      )
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./modules/home/home.module').then((m) => m.HomeModule)
  },
  {
    path: 'extra',
    loadChildren: () =>
      import('./modules/extra/extra.module').then((m) => m.ExtraModule)
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
