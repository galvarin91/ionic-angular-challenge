export interface ITeam {
  id: string;
  name: string;
  country: string;
  clubColors: string;
}

export interface ITeamDetail extends ITeam {
  address: string;
  website: string;
  founded: string;
  venue: string;
  members: ITeamMember[];
}

export interface ITeamMember {
  id: string;
  name: string;
  position: string;
  dateOfBirth: string;
  nationality: string;
}
