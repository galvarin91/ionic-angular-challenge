import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ICompetition } from '../models/competition';

interface IFilterData {
  seasonYear: string;
  showFreeTierData: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class CompetitionsService {
  private apiUrl = environment.apiUrl;
  private triggerSearch: Subject<IFilterData> = new Subject();

  constructor(private httpClient: HttpClient) {}

  get competitions$() {
    const firstValue: IFilterData = {
      seasonYear: '',
      showFreeTierData: false
    };

    return this.triggerSearch.pipe(
      //Intended to force an emit on subscribe without using a behavior subject
      startWith(firstValue),
      switchMap((filterData) => this.getCompetitions(filterData))
    );
  }

  private getCompetitions(filterData: IFilterData): Observable<ICompetition[]> {
    return this.httpClient.get(`${this.apiUrl}/competitions`).pipe(
      map((competitionsData: any) =>
        competitionsData.competitions.reduce((filtered, competition) => {
          const currentSeasonYear = new Date(
            competition.currentSeason?.startDate
          )
            .getFullYear()
            .toString();
          const { seasonYear, showFreeTierData } = filterData;

          if (
            currentSeasonYear.includes(seasonYear) &&
            ((showFreeTierData &&
              environment.availableCompetitionCodes.includes(
                competition.code
              )) ||
              !showFreeTierData)
          ) {
            const {
              id,
              name,
              area: { name: country }
            } = competition;
            filtered.push({ id, name, country, currentSeasonYear });
          }

          return filtered;
        }, [])
      )
    );
  }

  filterCompetitions(seasonYear: string, showFreeTierData: boolean) {
    this.triggerSearch.next({ seasonYear, showFreeTierData });
  }
}
