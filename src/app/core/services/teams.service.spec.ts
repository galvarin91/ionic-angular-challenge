import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { ITeam, ITeamDetail } from '../models/team';

import { TeamsService } from './teams.service';

describe('TeamsService', () => {
  let httpTestingController: HttpTestingController;
  let teamsService: TeamsService;
  const competitionId: string = '1';
  const seasonYear: string = '2020';
  const teamId: string = '1';
  const mockTeamsData = {
    teams: [
      {
        id: '1',
        name: 'team 1',
        area: { name: 'country 1' },
        clubColors: 'color 1',
        address: 'address 1',
        website: 'website 1',
        founded: '2001',
        venue: 'stadium 1',
        squad: [
          {
            id: '1',
            name: 'player 1',
            position: 'position 1',
            dateOfBirth: '01/01/21',
            nationality: 'country 1'
          },
          {
            id: '2',
            name: 'player 2',
            position: 'position 2',
            dateOfBirth: '02/02/21',
            nationality: 'country 2'
          }
        ]
      },
      {
        id: '2',
        name: 'team 2',
        area: { name: 'country 2' },
        clubColors: 'color 2'
      },
      {
        id: '3',
        name: 'team 3',
        area: { name: 'country 3' },
        clubColors: 'color 3'
      }
    ]
  };
  const mockTeamsResponse: ITeam[] = [
    {
      id: '1',
      name: 'team 1',
      country: 'country 1',
      clubColors: 'color 1'
    },
    {
      id: '2',
      name: 'team 2',
      country: 'country 2',
      clubColors: 'color 2'
    },
    {
      id: '3',
      name: 'team 3',
      country: 'country 3',
      clubColors: 'color 3'
    }
  ];
  const mockTeamDetailResponse: ITeamDetail = {
    id: '1',
    name: 'team 1',
    country: 'country 1',
    clubColors: 'color 1',
    address: 'address 1',
    website: 'website 1',
    founded: '2001',
    venue: 'stadium 1',
    members: [
      {
        id: '1',
        name: 'player 1',
        position: 'position 1',
        dateOfBirth: '01/01/21',
        nationality: 'country 1'
      },
      {
        id: '2',
        name: 'player 2',
        position: 'position 2',
        dateOfBirth: '02/02/21',
        nationality: 'country 2'
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TeamsService]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    teamsService = TestBed.inject(TeamsService);
  });

  it('should be created', () => {
    expect(teamsService).toBeTruthy();
  });

  it('should get teams', () => {
    teamsService.getTeams(competitionId, seasonYear).subscribe((teams) => {
      expect(teams).toEqual(mockTeamsResponse);
    });

    const req = httpTestingController.expectOne(
      `${environment.apiUrl}/competitions/${competitionId}/teams?season=${seasonYear}`
    );

    expect(req.request.method).toEqual('GET');
    req.flush(mockTeamsData);
  });

  it('should get team detail', () => {
    teamsService.getTeamDetail(teamId).subscribe((teamDetail) => {
      expect(teamDetail).toEqual(mockTeamDetailResponse);
    });

    const req = httpTestingController.expectOne(
      `${environment.apiUrl}/teams/${teamId}`
    );

    expect(req.request.method).toEqual('GET');
    req.flush(mockTeamsData.teams[0]);
  });
});
