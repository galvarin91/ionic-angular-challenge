import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ITeam, ITeamDetail, ITeamMember } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {
  private apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {}

  getTeams(competitionId: string, seasonYear: string): Observable<ITeam[]> {
    return this.httpClient
      .get(
        `${this.apiUrl}/competitions/${competitionId}/teams?season=${seasonYear}`
      )
      .pipe(
        map((teamsData: any) =>
          teamsData.teams.map((teamData) => {
            const {
              id,
              name,
              area: { name: country },
              clubColors
            } = teamData;

            return {
              id,
              name,
              country,
              clubColors
            };
          })
        )
      );
  }

  getTeamDetail(teamId: string): Observable<ITeamDetail> {
    return this.httpClient.get(`${this.apiUrl}/teams/${teamId}`).pipe(
      map((teamData: any) => {
        const {
          id,
          name,
          area: { name: country },
          address,
          clubColors,
          website,
          founded,
          venue,
          squad
        } = teamData;

        const members = this.prepareTeamMembers(squad);

        return {
          id,
          name,
          country,
          address,
          clubColors,
          website,
          founded,
          venue,
          members
        };
      })
    );
  }

  private prepareTeamMembers(squad: any[]): ITeamMember[] {
    return squad.map((member) => {
      const { id, name, position, dateOfBirth, nationality } = member;

      return { id, name, position, dateOfBirth, nationality };
    });
  }
}
