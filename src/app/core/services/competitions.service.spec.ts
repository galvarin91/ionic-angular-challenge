import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { CompetitionsService } from './competitions.service';
import { environment } from 'src/environments/environment';
import { skip } from 'rxjs/operators';
import { ICompetition } from '../models/competition';

describe('CompetitionsService', () => {
  let httpTestingController: HttpTestingController;
  let competitionsService: CompetitionsService;
  const mockCompetitionsData = {
    competitions: [
      {
        id: '1',
        name: 'competition 1',
        area: { name: 'country 1' },
        code: 'code1',
        currentSeason: { startDate: '01/01/2021' }
      },
      {
        id: '2',
        name: 'competition 2',
        area: { name: 'country 2' },
        code: environment.availableCompetitionCodes[0],
        currentSeason: { startDate: '01/01/2019' }
      },
      {
        id: '3',
        name: 'competition 3',
        area: { name: 'country 3' },
        code: 'code3',
        currentSeason: { startDate: '01/01/2019' }
      }
    ]
  };
  const mockCompetitionsResponse: ICompetition[] = [
    {
      id: '1',
      name: 'competition 1',
      country: 'country 1',
      currentSeasonYear: '2021'
    },
    {
      id: '2',
      name: 'competition 2',
      country: 'country 2',
      currentSeasonYear: '2019'
    },
    {
      id: '3',
      name: 'competition 3',
      country: 'country 3',
      currentSeasonYear: '2019'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CompetitionsService]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    competitionsService = TestBed.inject(CompetitionsService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(competitionsService).toBeTruthy();
  });

  it('should get competitions', () => {
    competitionsService.competitions$.subscribe((competitions) => {
      expect(competitions).toEqual(mockCompetitionsResponse);
    });

    const req = httpTestingController.expectOne(
      `${environment.apiUrl}/competitions`
    );

    expect(req.request.method).toEqual('GET');
    req.flush(mockCompetitionsData);
  });

  it('should get competitions with seasonYear === 2019', () => {
    const filteredCompetitions = mockCompetitionsResponse.filter(
      (competition) => competition.currentSeasonYear === '2019'
    );

    competitionsService.competitions$
      .pipe(skip(1))
      .subscribe((competitions) => {
        expect(competitions).toEqual(filteredCompetitions);
      });

    competitionsService.filterCompetitions('2019', false);

    const requests = httpTestingController.match(
      `${environment.apiUrl}/competitions`
    );
    expect(requests.length).toBe(2);

    expect(requests[0].request.method).toEqual('GET');
    expect(requests[1].request.method).toEqual('GET');
    requests[1].flush(mockCompetitionsData);
  });

  it('should get competitions with seasonYear === 2019 within the free tier', () => {
    const filteredCompetitions = [mockCompetitionsResponse[1]];

    competitionsService.competitions$
      .pipe(skip(1))
      .subscribe((competitions) => {
        expect(competitions).toEqual(filteredCompetitions);
      });

    competitionsService.filterCompetitions('2019', true);

    const requests = httpTestingController.match(
      `${environment.apiUrl}/competitions`
    );
    expect(requests.length).toBe(2);

    expect(requests[0].request.method).toEqual('GET');
    expect(requests[1].request.method).toEqual('GET');
    requests[1].flush(mockCompetitionsData);
  });
});
